import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Form from './views/Form';
import List from './views/List';
import Search from './views/Search';
import Suggestion from './views/Suggestion';


function App() {
  return (
    <div>
      <Router>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="#">Lojao</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item active">
              <a   className="nav-link" href="#">Populares <span className="sr-only">(current)</span></a>

            </li>
            
            <li className="nav-item">
              <a className="nav-link" href="#">Preços</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Gêneros</a>            </li>
          </ul>
        </div>
      </nav>


      {/*Menu vertical */}

      <div className='row'>
        <div className='col-2'>
          <ul className="nav flex-column">
           
            <li className="nav-item">
              <Link to="/form">Conta</Link>
            </li>
            <li className="nav-item">
              <Link to="/lista/games">Lista de Games</Link>
            </li>
            <li className="nav-item">
              <Link to="/pesquisa">Pesquisa</Link>
            </li>
            <li className="nav-item">
              <Link to="/sugerir">Sugerir</Link>
            </li>

            </ul>


        </div>
        {/*Conteúdo dinamico */}
        <div className='col-10'>
        
          <Switch>
          <Route path="/lista/games" component={List}></Route>
          <Route path="/form" component={Form}></Route>
          <Route path="/pesquisa" component={Search}></Route>
          <Route path="/sugerir" component={Suggestion}></Route>


          </Switch>
        </div>

      </div>


      </Router>
    </div>
  );
}

export default App;
